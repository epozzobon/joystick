// SPDX-License-Identifier: GPL-2.0
/*
 * Joystick firmware based on the STM32F103C8T6.
 * Released under the GPL 2.0 license.
 * Based mostly on the libopencm3 usb hid demo:
 * https://github.com/libopencm3/libopencm3-examples/blob/master/examples/stm32/f1/other/usb_hid/usbhid.c
 *
 * Copyright (C) 2022 Enrico Pozzobon <enrico@epozzobon.it>
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/hid.h>

#define SWITCH_SUPPORT

#define PACKET_SIZE 8

#define HID_POV_HAT_IDLE 8
#define HID_POV_HAT_NORTH 0
#define HID_POV_HAT_NORTH_EAST 1
#define HID_POV_HAT_EAST 2
#define HID_POV_HAT_SOUTH_EAST 3
#define HID_POV_HAT_SOUTH 4
#define HID_POV_HAT_SOUTH_WEST 5
#define HID_POV_HAT_WEST 6
#define HID_POV_HAT_NORTH_WEST 7
#define SOCD HID_POV_HAT_IDLE

static const int8_t lookup_dpad_to_hat[16] = {
		HID_POV_HAT_IDLE,
		HID_POV_HAT_NORTH,
		HID_POV_HAT_SOUTH,
		SOCD,                    // North+South
		HID_POV_HAT_WEST,
		HID_POV_HAT_NORTH_WEST,  // North+West
		HID_POV_HAT_SOUTH_WEST,  // South+West
		HID_POV_HAT_WEST,        // North+South+West
		HID_POV_HAT_EAST,
		HID_POV_HAT_NORTH_EAST,  // North+East
		HID_POV_HAT_SOUTH_EAST,  // South+East
		HID_POV_HAT_EAST,        // East+North+South
		SOCD,                    // East+West
		HID_POV_HAT_NORTH,       // East+West+North
		HID_POV_HAT_SOUTH,       // East+West+South
		SOCD,                    // North+East+West+South
};

struct physical_inputs {
	uint8_t dpad;
	uint16_t buttons;
	uint8_t switches;
};

static usbd_device *usbd_dev;
static bool initialized = false;

const struct usb_device_descriptor dev_descr = {
	.bLength = USB_DT_DEVICE_SIZE,
	.bDescriptorType = USB_DT_DEVICE,
	.bcdUSB = 0x0200,
	.bDeviceClass = 0,
	.bDeviceSubClass = 0,
	.bDeviceProtocol = 0,
	.bMaxPacketSize0 = 64,
#ifdef SWITCH_SUPPORT
	/* vid and pid from github.com/soonuse/stm32_joystick_for_nintendo_switch */
	.idVendor = 0x0f0d,
	.idProduct = 0x0092,
#else
	/* Default vid and pid from ST */
    .idVendor = 0x0483,
    .idProduct = 0x5710,
#endif
	.bcdDevice = 0x0001,
	.iManufacturer = 1,
	.iProduct = 2,
	.iSerialNumber = 0,
	.bNumConfigurations = 1,
};

static const uint8_t hid_report_descriptor[] = {
	0x05, 0x01,        // Usage Page (Generic Desktop Ctrls)
	0x09, 0x05,        // Usage (Game Pad)
	0xA1, 0x01,        // Collection (Application)
	0x15, 0x00,        //   Logical Minimum (0)
	0x25, 0x01,        //   Logical Maximum (1)
	0x35, 0x00,        //   Physical Minimum (0)
	0x45, 0x01,        //   Physical Maximum (1)
	0x75, 0x01,        //   Report Size (1)
	0x95, 0x10,        //   Report Count (16)
	0x05, 0x09,        //   Usage Page (Button)
	0x19, 0x01,        //   Usage Minimum (0x01)
	0x29, 0x10,        //   Usage Maximum (0x10)
	0x81, 0x02,        //   Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
	0x05, 0x01,        //   Usage Page (Generic Desktop Ctrls)
	0x25, 0x07,        //   Logical Maximum (7)
	0x46, 0x3B, 0x01,  //   Physical Maximum (315)
	0x75, 0x04,        //   Report Size (4)
	0x95, 0x01,        //   Report Count (1)
	0x65, 0x14,        //   Unit (System: English Rotation, Length: Centimeter)
	0x09, 0x39,        //   Usage (Hat switch)
	0x81, 0x42,        //   Input (Data,Var,Abs,No Wrap,Linear,Preferred State,Null State)
	0x65, 0x00,        //   Unit (None)
	0x95, 0x01,        //   Report Count (1)
	0x81, 0x01,        //   Input (Const,Array,Abs,No Wrap,Linear,Preferred State,No Null Position)
	0x26, 0xFF, 0x00,  //   Logical Maximum (255)
	0x46, 0xFF, 0x00,  //   Physical Maximum (255)
	0x09, 0x30,        //   Usage (X)
	0x09, 0x31,        //   Usage (Y)
	0x09, 0x32,        //   Usage (Z)
	0x09, 0x35,        //   Usage (Rz)
	0x75, 0x08,        //   Report Size (8)
	0x95, 0x04,        //   Report Count (4)
	0x81, 0x02,        //   Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
	0x06, 0x00, 0xFF,  //   Usage Page (Vendor Defined 0xFF00)
	0x09, 0x20,        //   Usage (0x20)
	0x95, 0x01,        //   Report Count (1)
	0x81, 0x02,        //   Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
	0x05, 0x08,        //   Usage Page (LEDs)
	0x09, 0x43,        //   Usage (Slow Blink On Time)
	0x15, 0x00,        //   Logical Minimum (0)
	0x26, 0xFF, 0x00,  //   Logical Maximum (255)
	0x35, 0x00,        //   Physical Minimum (0)
	0x46, 0xFF, 0x00,  //   Physical Maximum (255)
	0x75, 0x08,        //   Report Size (8)
	0x95, 0x02,        //   Report Count (2)
	0x91, 0x82,        //   Output (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Volatile)
	0x09, 0x44,        //   Usage (Slow Blink Off Time)
	0x91, 0x82,        //   Output (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Volatile)
	0x09, 0x45,        //   Usage (Fast Blink On Time)
	0x91, 0x82,        //   Output (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Volatile)
	0x09, 0x46,        //   Usage (Fast Blink Off Time)
	0x91, 0x82,        //   Output (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Volatile)
	0xC0,              // End Collection
};

static const struct {
	struct usb_hid_descriptor hid_descriptor;
	struct {
		uint8_t bReportDescriptorType;
		uint16_t wDescriptorLength;
	} __attribute__((packed)) hid_report;
} __attribute__((packed)) hid_function = {
	.hid_descriptor = {
		.bLength = sizeof(hid_function),
		.bDescriptorType = USB_DT_HID,
		.bcdHID = 0x0111,
		.bCountryCode = 0,
		.bNumDescriptors = 1,
	},
	.hid_report = {
		.bReportDescriptorType = USB_DT_REPORT,
		.wDescriptorLength = sizeof(hid_report_descriptor),
	}
};

const struct usb_endpoint_descriptor hid_endpoints[2] = {
	{
		.bLength = USB_DT_ENDPOINT_SIZE,
		.bDescriptorType = USB_DT_ENDPOINT,
		.bEndpointAddress = 0x81,
		.bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,
		.wMaxPacketSize = 64,
		.bInterval = 1,
	}, {
		.bLength = USB_DT_ENDPOINT_SIZE,
		.bDescriptorType = USB_DT_ENDPOINT,
		.bEndpointAddress = 0x02,
		.bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,
		.wMaxPacketSize = 64,
		.bInterval = 16,
	}
};

const struct usb_interface_descriptor hid_iface = {
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = 0,
	.bAlternateSetting = 0,
	.bNumEndpoints = 2,
	.bInterfaceClass = USB_CLASS_HID,
	.bInterfaceSubClass = 0,
	.bInterfaceProtocol = 0,
	.iInterface = 0,

	.endpoint = hid_endpoints,

	.extra = &hid_function,
	.extralen = sizeof(hid_function),
};

const struct usb_interface ifaces[] = {{
	.num_altsetting = 1,
	.altsetting = &hid_iface,
}};

const struct usb_config_descriptor config = {
	.bLength = USB_DT_CONFIGURATION_SIZE,
	.bDescriptorType = USB_DT_CONFIGURATION,
	.wTotalLength = 41,
	.bNumInterfaces = 1,
	.bConfigurationValue = 1,
	.iConfiguration = 0,
	.bmAttributes = 0x80,
	.bMaxPower = 240,

	.interface = ifaces,
};

static const char *usb_strings[] = {
	"Olifante",
	"Olifante's Stick",
	"Stick",
};

/* Buffer to be used for control requests. */
static uint8_t usbd_control_buffer[128];

static uint8_t data[0x40];
static uint8_t olddata[0x40];

static enum usbd_request_return_codes hid_control_request(usbd_device *dev, struct usb_setup_data *req, uint8_t **buf, uint16_t *len,
			void (**complete)(usbd_device *, struct usb_setup_data *))
{
	(void)complete;
	(void)dev;

	if((req->bmRequestType != 0x81) ||
	   (req->bRequest != USB_REQ_GET_DESCRIPTOR) ||
	   (req->wValue != 0x2200))
		return USBD_REQ_NOTSUPP;

	/* Handle the HID report descriptor. */
	*buf = (uint8_t *)hid_report_descriptor;
	*len = sizeof(hid_report_descriptor);

	return USBD_REQ_HANDLED;
}

static void usb_sof_callback(void)
{
	/* Assuming SOF comes exactly every 1000us, sample state of GPIOs 200us
	before the next USB SOF. This should reduce latency. */
	timer_set_counter(TIM2, 1);
	timer_enable_counter(TIM2);
	gpio_set(GPIOB, GPIO0);
}

static void hid_set_config(usbd_device *dev, uint16_t wValue)
{
	(void)wValue;
	(void)dev;

	usbd_ep_setup(dev, 0x81, USB_ENDPOINT_ATTR_INTERRUPT, PACKET_SIZE, NULL);

	usbd_register_control_callback(
			dev,
			USB_REQ_TYPE_STANDARD | USB_REQ_TYPE_INTERFACE,
			USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
			hid_control_request);

	initialized = true;
}

#define BIND_BUTTON(port, pin, nr) (inputs_##port & GPIO##pin ? (1 << (nr)) : 0)
static struct physical_inputs fetch_inputs(void) {
	struct physical_inputs result;

	uint16_t inputs_A = ~gpio_get(GPIOA, 0xffff);
	uint16_t inputs_B = ~gpio_get(GPIOB, 0xffff);
	uint16_t inputs_C = ~gpio_get(GPIOC, 0xffff);

	result.dpad = 0
			| BIND_BUTTON(B, 6 , 0)  // Up
		    | BIND_BUTTON(B, 7 , 1)  // Down
			| BIND_BUTTON(B, 4 , 2)  // Left
			| BIND_BUTTON(B, 5 , 3)  // Right
		    ;

	result.buttons = 0
			| BIND_BUTTON(B, 3 , 0)
			| BIND_BUTTON(B, 13, 1)
			| BIND_BUTTON(B, 15, 2)
			| BIND_BUTTON(B, 14, 3)
			| BIND_BUTTON(A, 8 , 4)
			| BIND_BUTTON(A, 9 , 5)
			| BIND_BUTTON(A, 10, 6)
			| BIND_BUTTON(A, 15, 7)
		    | BIND_BUTTON(B, 11, 8)
		    | BIND_BUTTON(B, 10, 9)
			| BIND_BUTTON(B, 1 ,10)
			| BIND_BUTTON(B, 0 ,11)
			| BIND_BUTTON(A, 7 ,12)
			| BIND_BUTTON(A, 6 ,13)
			| BIND_BUTTON(A, 5 ,14)
			| BIND_BUTTON(A, 4 ,15)
			;

	result.switches = 0
			| BIND_BUTTON(B,  2, 0)
			;

	uint8_t unused = 0
			| BIND_BUTTON(B,  8, 1)
			| BIND_BUTTON(B,  9, 2)
			| BIND_BUTTON(C, 13, 3)
			| BIND_BUTTON(A,  3, 4)
			| BIND_BUTTON(A,  2, 5)
			| BIND_BUTTON(A,  1, 6)
			| BIND_BUTTON(A,  0, 7)
			;
	(void) unused;

	return result;
}

static void craft_usb_packet(uint8_t *paket, struct physical_inputs inputs) {
	uint8_t dpad = inputs.dpad;
	uint16_t buttons = inputs.buttons;

	uint8_t axis_x = 0x80;
	uint8_t axis_y = 0x80;
	uint8_t hat = HID_POV_HAT_IDLE;
	if (inputs.switches & 1) {
		// Direct dpad to X, Y
		axis_x = (dpad & 4) ? 0x00 : ((dpad & 8) ? 0xff : 0x80);
		axis_y = (dpad & 1) ? 0x00 : ((dpad & 2) ? 0xff : 0x80);
	} else {
		// Direct dpad to hat
		hat = lookup_dpad_to_hat[dpad & 0xf] & 0xf;
	}

	paket[0] = buttons & 0xff;
	paket[1] = buttons >> 8;
	paket[2] = hat;
	paket[3] = axis_x;  // axis X
	paket[4] = axis_y;  // axis Y
	paket[5] = 0x80;  // axis Z
	paket[6] = 0x80;  // axis Rz
	paket[7] = 0;  // Vendor defined
}

int main(void)
{
	rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);

	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_GPIOC);
	rcc_periph_clock_enable(RCC_AFIO);
	rcc_periph_clock_enable(RCC_USB);
	rcc_periph_clock_enable(RCC_TIM2);

	/* Disable JTAG to use more input pins */
	gpio_primary_remap(AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON, AFIO_MAPR_TIM2_REMAP_NO_REMAP);

	/* Setup GPIOB Pin 12 for the LED */
	gpio_set(GPIOB, GPIO12);
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ,
			GPIO_CNF_OUTPUT_PUSHPULL, GPIO12);
	
	/* Setup input GPIOs */
	gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN,
			GPIO0 | GPIO1 | GPIO2 | GPIO3 | GPIO4 | GPIO5 | GPIO6 | GPIO7 |
			GPIO8 | GPIO9 | GPIO10 | GPIO15);
	gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN,
			GPIO0 | GPIO1 | GPIO2 | GPIO3 | GPIO4 | GPIO5 | GPIO6 | GPIO7 |
			GPIO8 | GPIO9 | GPIO10 | GPIO11 | GPIO13 | GPIO14 | GPIO15);
	gpio_set_mode(GPIOC, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN,
			GPIO13 | GPIO14 | GPIO15);

	gpio_set(GPIOA,
			GPIO0 | GPIO1 | GPIO2 | GPIO3 | GPIO4 | GPIO5 | GPIO6 | GPIO7 |
			GPIO8 | GPIO9 | GPIO10 | GPIO15);
	gpio_set(GPIOB,
			GPIO0 | GPIO1 | GPIO3 | GPIO4 | GPIO5 | GPIO6 | GPIO7 | GPIO8 |
			GPIO9 | GPIO10 | GPIO11 | GPIO13 | GPIO14 | GPIO15);
	gpio_set(GPIOC, GPIO13 | GPIO14 | GPIO15);

	/* PA13 PA14 are used for SWD */
	/* PA11 PA12 are used for USB */
	/* PB12 is used for activity LED */

	/* Setup TIM2 to timeout in 500ms */
	nvic_enable_irq(NVIC_TIM2_IRQ);
	rcc_periph_reset_pulse(RST_TIM2);
	/* Set timer prescaler. 72MHz/1440 => 50000 counts per second. */
	timer_set_prescaler(TIM2, 1440);
	timer_set_period(TIM2, 40);  // 40 counts -> 800 us
	timer_enable_irq(TIM2, TIM_DIER_UIE);
	timer_one_shot_mode(TIM2);

	memset(olddata, 0xff, PACKET_SIZE);
	memset(data, 0, PACKET_SIZE);

	// "Software" USB reset
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ,
			GPIO_CNF_OUTPUT_PUSHPULL, GPIO12);
	for (unsigned i = 0; i < 800000; i++) {
		__asm__("nop");
	}

	usbd_dev = usbd_init(&st_usbfs_v1_usb_driver, &dev_descr, &config, usb_strings, 3, usbd_control_buffer, sizeof(usbd_control_buffer));
	usbd_register_set_config_callback(usbd_dev, hid_set_config);
	usbd_register_sof_callback(usbd_dev, usb_sof_callback);

	while (1) {
		usbd_poll(usbd_dev);
	}
}

void tim2_isr(void) {
	// sample 800 us after the last USB SOF, 200us before the next
	TIM_SR(TIM2) &= ~TIM_SR_UIF;

	if (initialized) {
		struct physical_inputs inputs = fetch_inputs();
		craft_usb_packet(data, inputs);
		if (usbd_ep_write_packet(usbd_dev, 0x81, data, PACKET_SIZE)) {
			memcpy(olddata, data, PACKET_SIZE);
			gpio_set(GPIOB, GPIO12);
		} else {
			// LED reports the error
			gpio_clear(GPIOB, GPIO12);
		}
	}
	gpio_clear(GPIOB, GPIO0);
}
